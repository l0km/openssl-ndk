# openssl-ndk

NDK 编译 openssl 脚本

## 下载源码

	# 添加 --recursive 参数下载opensl源码
	git clone --recursive https://gitee.com/l0km/openssl-ndk.git

## 文件说明

	├── build.bat 	  ## [未完成]bat脚本,编译所有cpu版本
	├── build.sh 	  ## bash脚本,编译所有cpu版本
	├── ndk-build.bat ## [未完成]bat脚本,编译指定cpu(armeabi,armeabi-v7a,arm64-v8a,x86,x86_64)的版本,需要perl支持
	├── ndk-build.sh  ## bash脚本,编译指定cpu(armeabi,armeabi-v7a,arm64-v8a,x86,x86_64)的版本,Windows可在MSYS2下执行
	└── openssl       ## openssl官方源码git仓库 (git submodule)
## 编译要求

目前在Android NDK 16b上测试通过，因为NDK 19不再支持gcc编译,所以19以上版本不可用

Windows下执行`ndk-build.bat`需要安装perl支持

Windows下在MSYS2可直接执行`ndk-build.sh`

建议linux下编译
## 参数说明
  `ndk-build.bat`,`ndk-build.sh`脚本可以通过环境变量来控制生成的版本

环境变量参数说明:

- TAG 指定编译openssl 版本
	
	默认编译openssl `1.1.1d`版本,可以通过指定此环境变量定义openssl git分支来编译指定的版本,如 `OpenSSL_1_1_1h`指定`1.1.1h`版本

- ANDROID_ABI 处理器

	默认编译`armeabi-v7a`处理器版本,可以通过指定此环境变量来指定要编译的CPU平台

- PREFIX 安装路径

	默认编译安装路径为`$openssl-ndk/dist/openssl_android/$ANDROID_ABI`

- OPTIONS openssl编译选项

	默认选项为 `no-shared no-asm no-ssl2 no-ssl3 no-comp no-hw no-engine`,可以设置此环境变量覆盖默认定义


