#!/bin/bash
# NDK下交叉编译 openssl 库(armeabi,armeabi-v7a,arm64-v8a,x86,x86_64)

ANDROID_ABI=armeabi ./ndk-build.sh || exit
ANDROID_ABI=armeabi-v7a ./ndk-build.sh || exit
ANDROID_ABI=arm64-v8a ./ndk-build.sh || exit
ANDROID_ABI=x86 ./ndk-build.sh || exit
ANDROID_ABI=x86_64 ./ndk-build.sh || exit