
:: NDK 编译openssl 
:: NDK 版本要求 16 
:: 如果不指定默认编译 OpenSSL_1_1_1d 版本 

@ECHO OFF
SETLOCAL
SET sh_folder=%~dp0

:: 检测是否安装NDK,没有安装NDK则报错退出
IF NOT DEFINED ANDROID_NDK (
    ECHO "ERROR:environment variable ANDROID_NDK not defined,NDK 16 required" 
    EXIT /B 255
    )

IF NOT EXIST "%ANDROID_NDK%" (
    ECHO "ERROR:invalid environment variable ANDROID_NDK=%ANDROID_NDK%" 
    EXIT /B 255
    )

:: 目标平台
IF NOT DEFINED ANDROID_ABI SET ANDROID_ABI=armeabi-v7a

IF "%ANDROID_ABI%"=="armeabi" ( 
  SET compiler_folder=arm-linux-androideabi-4.9
  SET ANDROID_API=14
  SET toolset=android-arm
) ELSE IF "%ANDROID_ABI%"=="armeabi-v7a" ( 
  SET compiler_folder=arm-linux-androideabi-4.9
  SET ANDROID_API=14
  SET toolset=android-arm
) ELSE IF "%ANDROID_ABI%"=="arm64-v8a" (
  SET scompiler_folder=aarch64-linux-android-4.9
  SET ANDROID_API=21
  SET toolset=android-arm64
) ELSE IF "%ANDROID_ABI%"=="x86" (
  SET compiler_folder=x86-4.9
  SET ANDROID_API=14
  SET toolset=android-x86
) ELSE IF "%ANDROID_ABI%"=="x86_64" (
  SET compiler_folder=x86_64-4.9
  SET ANDROID_API=21
  SET toolset=android-x86_64
) ELSE (
    ECHO "Invalid Android ABI: %ANDROID_ABI%." 
    EXIT /B 255
)

SET compiler_path=%ANDROID_NDK%\toolchains\%compiler_folder%\prebuilt\windows-x86_64\bin
IF NOT EXIST "%compiler_path%" (
    ECHO "ERROR:invalid compiler path %compiler_path% for ANDROID_ABI=%ANDROID_ABI%" 
    EXIT /B 255
    )
SET PATH=%compiler_path%;%PATH%

:: openssl 源码位置
IF "%OPENSSL_FOLDER%x" == "x" SET OPENSSL_FOLDER=%sh_folder%openssl

:: openssl 版本号 
IF "%TAG%x" == "x" SET TAG=OpenSSL_1_1_1d
:: 安装路径 
IF "%PREFIX%x" == "x"  SET PREFIX=%sh_folder%dist\openssl_android\%ANDROID_ABI%
:: 编译选项 
IF "%OPTIONS%x" == "x" SET OPTIONS=shared no-asm no-ssl2 no-ssl3 no-comp no-hw no-engine

ECHO =========================================================================
ECHO     Configuration   : openssl %ANDROID_ABI%
ECHO         ANDROID_API : %ANDROID_API%
ECHO             Options : %OPTIONS%
ECHO   Compiler Directory: %compiler_path%
ECHO Install Directory   : %PREFIX%
ECHO             Version : %TAG%
ECHO =========================================================================

PUSHD %OPENSSL_FOLDER% || EXIT /B
IF "%OPENSSL_FOLDER%"=="%sh_folder%openssl" ( 
    git checkout %TAG% || EXIT /B
)

perl ./Configure %toolset% -D__ANDROID_API__=%ANDROID_API% %OPTIONS% --prefix=%PREFIX% || EXIT /B
make clean || EXIT /B
make -j8 || EXIT /B
make install || EXIT /B

POPD
ENDLOCAL
