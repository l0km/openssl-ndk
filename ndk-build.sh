#!/bin/bash
# NDK下交叉编译openssl 库
# 
# 环境变量:
# ANDROID_NDK : ANDROID NDK位置,NDK版本要求 16 及以下
# ANDROID_ABI : 目标平台,[armeabi,armeabi-v7a,arm64-v8a,x86,x86_64],默认 armeabi-v7a
# TAG : 指定要编译的openssl git 分支,如果不指定默认编译 OpenSSL_1_1_1d 版本
# OPENSSL_FOLDER : openssl 源码位置,默认为当前文件夹下的 openssl
# PREFIX : 安装路径,默认为 当前文件夹下 dist/openssl_android/$ANDROID_ABI
# OPTIONS : openssl编译选项,默认为 no-shared no-asm no-ssl2 no-ssl3 no-comp no-hw no-engine

sh_folder=$(dirname $(readlink -f $0))

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     buildPlatform=linux-x86_64;;
    Darwin*)    buildPlatform=darwin-x86;;
    CYGWIN*)    buildPlatform=windows-x86_64;;
    MINGW*)     buildPlatform=windows-x86_64;;
    *)          echo "UNSUPPORTED PLATFORM $unameOut" && exit 255;;
esac
echo buildPlatform=${buildPlatform}

[ -z "$ANDROID_NDK" ] && export ANDROID_NDK=/d/j/android-ndk-r16b

# 检测是否安装NDK,没有安装NDK则报错退出
[ ! -d "$ANDROID_NDK" ] && echo "ERROR:environment variable ANDROID_NDK not define" && exit 255

# 从 source.properties NDK 版本号
NDK_VERSION=$(cat $ANDROID_NDK/source.properties | grep Pkg\.Revision | sed -E 's/\./ /g' | awk '{print $4}')

echo "Android NDK Version:$NDK_VERSION"

# 目标平台
[ -z "$ANDROID_ABI" ] && ANDROID_ABI=armeabi-v7a

# NDK版本要求 16 及以下,否则退出
[ "$NDK_VERSION" -gt 16 ] && echo "Android NKD r16 or below required" && exit 255


if [ "$ANDROID_ABI" = "armeabi" ]
then
  compiler_folder=arm-linux-androideabi-4.9
  ANDROID_API=14
  toolset=android-arm
elif [ "$ANDROID_ABI" = "armeabi-v7a" ]
then
  compiler_folder=arm-linux-androideabi-4.9
  ANDROID_API=14
  toolset=android-arm
elif [ "$ANDROID_ABI" = "arm64-v8a" ]
then
  compiler_folder=aarch64-linux-android-4.9
  ANDROID_API=21
  toolset=android-arm64
elif [ "$ANDROID_ABI" = "x86" ]
then
  compiler_folder=x86-4.9
  ANDROID_API=14
  toolset=android-x86
elif [ "$ANDROID_ABI" = "x86_64" ]
then
  compiler_folder=x86_64-4.9
  ANDROID_API=21
  toolset=android-x86_64
else
  echo "Invalid Android ABI: ${ANDROID_ABI}." 
  exit 255
fi
compiler_path=$ANDROID_NDK/toolchains/$compiler_folder/prebuilt/$buildPlatform/bin
[ ! -d "$compiler_path" ] && echo "ERROR:invalid compiler path $compiler_path for ANDROID_ABI=$ANDROID_ABI"  && exit 255
export PATH=$compiler_path:$PATH

# openssl 源码位置
[ -z "$OPENSSL_FOLDER" ] && OPENSSL_FOLDER=$sh_folder/openssl
# openssl 版本号
[ -z "$TAG" ] && TAG=OpenSSL_1_1_1d
# 安装路径
[ -z "$PREFIX" ] && PREFIX=$sh_folder/dist/openssl_android/$ANDROID_ABI
# 编译选项
[ -z "$OPTIONS" ] && OPTIONS="no-shared no-asm no-ssl2 no-ssl3 no-comp no-hw no-engine"

echo =========================================================================
echo     Configuration   : openssl $ANDROID_ABI 
echo         ANDROID_API : $ANDROID_API
echo             Options : $OPTIONS
echo   Compiler Directory: $compiler_path
echo Install Directory   : $PREFIX
echo             Version : $TAG
echo =========================================================================

pushd $OPENSSL_FOLDER || exit 
if [ "$OPENSSL_FOLDER" = "$sh_folder/openssl" ]
then
    git checkout $TAG || exit 
fi
./Configure $toolset -D__ANDROID_API__=$ANDROID_API $OPTIONS --prefix=$PREFIX || exit 
make clean || exit
make -j8 && make install

popd